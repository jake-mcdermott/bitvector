#ifndef BITVECTOR_H
#define BITVECTOR_H

#include <cstdint>

class BitVector {

private:	
	uint64_t mem_size;
	uint64_t mem_size_bits;
	uint64_t maxSize;
	uint8_t * data;
	uint8_t arr_size;

public:
	BitVector(uint64_t);
	void setTrue(uint64_t inData);
	void setFalse(uint64_t inData);
	bool get(uint64_t inData);
	void printBits() const;
	const BitVector& operator=(const BitVector& inData);
	uint8_t * getArray() const;
	uint8_t getArrSize() const;
	uint64_t getSize() const;
};

#endif