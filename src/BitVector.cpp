#include "BitVector.h"

#include <iostream>
#include <bitset>
#include <stdexcept>

BitVector::BitVector(uint64_t size){

	// Number of bits for every array slot
	mem_size = 8;
	//Number of bits to represent 8, including the 0th bit. 
	mem_size_bits = 3;
	// Subtract 1 to account for 0th bit, ie Vector of size 32 holds 0 - 31 bits. 
	maxSize = size - 1;
	// Determining how many 8 bit slots we need to represent our BitVector and creating the array.
	arr_size = maxSize/mem_size + 1;
	data = new uint8_t[arr_size];
}

void BitVector::setTrue(uint64_t inData){
		
	if(inData <= maxSize){

		data[inData >> mem_size_bits] |= (1 << (inData % mem_size));
	}
	else {
		
		throw std::range_error("BitVector range exception");
	}
}

void BitVector::setFalse(uint64_t inData){

	if(inData <= maxSize){

		data[inData >> mem_size_bits] &= ~(1 << (inData % mem_size));
	}
	else {
		
		throw std::range_error("BitVector range exception");
	}
}

bool BitVector::get(uint64_t inData){

	if(inData <= maxSize){

		return data[inData >> mem_size_bits] & (1 << (inData % mem_size));
	}
	else {
		
		throw std::range_error("BitVector range exception");
	}
}

void BitVector::printBits() const {

	// In order to print the bits from right to left, we begin printing the bits from the end of the array
	for(int i = arr_size-1; i >= 0; i--){
		// Taking a bitset of 8 bits, which is the size of the 8 bit integers in the array
		std::bitset<8> x(data[i]);
		std::cout << x << " ";
	}
	std::cout << std::endl;
}

uint8_t * BitVector::getArray() const {

	return data;
}

uint8_t BitVector::getArrSize() const{

	return arr_size;
}

uint64_t BitVector::getSize() const{

	return maxSize;
}

const BitVector& BitVector::operator=(const BitVector& inData){

	if(this == &inData) return *this;
	delete[] data;
	maxSize = inData.getSize();
	arr_size = inData.getArrSize();
	data = new uint8_t[arr_size];
	data = inData.getArray();
	return *this;
}
