#include "BitVector.h"

#include <iostream>
#include <math.h>

void eratosthenesSieve(uint64_t upperBound){

	int upperBoundSquareRoot = (int)sqrt((double)upperBound);
    BitVector bv(upperBound + 1);
    
    for (int m = 2; m <= upperBoundSquareRoot; m++) {
        if(bv.get(m) == 0){
        	for (int k = m*2; k <= upperBound; k += m){
            	bv.setTrue(k);
        	}
        }
    }
    
    bv.printBits();
    for (int m = 2; m <= upperBound; m++){
		if (bv.get(m) == 0){
            std::cout << m << " ";
		}
    }
    std::cout << std::endl;
}

int main(){

	eratosthenesSieve(100);

	BitVector bv1(100);
	BitVector bv2(50);
	bv2 = bv1;
}
